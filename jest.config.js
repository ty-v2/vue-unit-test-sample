module.exports = {
    moduleFileExtensions: [
        'ts',
        'js',
        'jsx',
        'tsx',
        'vue',
    ],
    moduleNameMapper: {
        '^@/(.+)': '<rootDir>/src/$1'
    },
    transform: {
        '^.*\\.vue$': 'vue-jest',
        '^.+\\.tsx?$': 'ts-jest',
        '^.+\\.jsx?$': 'babel-jest',
    },
    transformIgnorePatterns: [
        // '<rootDir>/node_modules/(?!vee-validate/dist/rules)',
    ],
    globals: {
        'ts-jest': {
            tsConfig: 'tsconfig.json'
        }
    },
    testMatch: [
        '**/__tests__/**/*.spec.ts'
    ],
    setupFilesAfterEnv: ['<rootDir>/src/__tests__/setupTests.ts'],
};
