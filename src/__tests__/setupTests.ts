import '@testing-library/jest-dom';
import '@/validationRules';
import Vue from 'vue';
import Vuetify from 'vuetify';

Vue.use(Vuetify);
