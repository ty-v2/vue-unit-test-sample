import {fireEvent, render, screen} from '@testing-library/vue';
import SampleForm from '@/components/SampleForm.vue';
import {flush} from '@/__tests__/util';

jest.useFakeTimers();

describe('with invalid values', () => {
    test('renders without alert', () => {
        render(SampleForm);
        expect(screen.queryAllByRole('alert')).toHaveLength(0);
    });

    test('does not emit event when click submit button', async () => {
        const {emitted} = render(SampleForm);

        fireEvent.click(screen.getByText('Submit'));
        await flush();

        expect(screen.getAllByRole('alert')).not.toHaveLength(0);
        expect(emitted().submit).toBeUndefined();
    });
});
