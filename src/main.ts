import Vue from 'vue';
import App from './App.vue';
import {ValidationObserver, ValidationProvider} from 'vee-validate';
import './validationRules';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

new Vue({
    vuetify,
    render: h => h(App)
}).$mount('#app');
