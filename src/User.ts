export type User = {
    readonly name: string;
    readonly email: string;
};
