import {extend} from 'vee-validate';
import {email, max, required} from 'vee-validate/dist/rules';

extend('required', {
    ...required,
    message: 'This field is required'
});

extend('email', {
    ...email,
    message: 'This field must be a valid email'
});

extend('max', {
    ...max,
    message: 'The field must have {length} characters at most'
});
